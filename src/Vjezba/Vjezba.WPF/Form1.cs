﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vjezba.DAL;

namespace Vjezba.WPF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            CompanyDTO company = new CompanyDTO()
            {
                Name = nazivTextBox.Text,
                Address = addressTextBox.Text,
                CityID = Int32.Parse(cityIdBox.Value.ToString()),
                DateFrom = dateTimePicker1.Value,
                Email = emailTextBox.Text,
                Latitude = latitudeBox.Value,
                Longitude = longitudeBox.Value
            };
            using (var client = new HttpClient())
            {
                HttpResponseMessage response =
                    await client.PostAsJsonAsync("http://localhost:51018/" + "api/company/add", company);
                response.EnsureSuccessStatusCode();
            }
        }
    }
}
