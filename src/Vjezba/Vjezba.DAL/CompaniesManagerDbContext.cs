﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using Vjezba.Model;

namespace Vjezba.DAL
{
    public class CompaniesManagerDbContext : IdentityDbContext<User>
    {
        public CompaniesManagerDbContext()
            : base("CompaniesManagerDbContext", throwIfV1Schema: false)
        {
        }

        public static CompaniesManagerDbContext Create()
        {
            return new CompaniesManagerDbContext();
        }

        public DbSet<Company> Companies { get; set; }

        public DbSet<City> Cities { get; set; }
    }
}