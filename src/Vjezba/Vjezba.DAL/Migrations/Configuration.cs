namespace Vjezba.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Vjezba.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<Vjezba.DAL.CompaniesManagerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Vjezba.DAL.CompaniesManagerDbContext context)
        {
            context.Cities.AddOrUpdate(p => p.ID,
                new City() { ID = 1, Name = "Zagreb", PostalCode = "10000", DateCreated = DateTime.Now },
                new City() { ID = 2, Name = "Split", PostalCode = "21000", DateCreated = DateTime.Now },
                new City() { ID = 3, Name = "Rijeka", PostalCode = "23000", DateCreated = DateTime.Now });



            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
