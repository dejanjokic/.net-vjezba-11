﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL.Repository
{
    public class CityRepository : RepositoryBase<City>
    {
        public CityRepository(CompaniesManagerDbContext context)
            : base(context)
        {
        }

        public List<City> GetList()
        {
            return this.DbContext.Cities
                .OrderBy(p => p.ID)
                .ToList();
        }
    }
}
