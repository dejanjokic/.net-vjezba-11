﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vjezba.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Kompanija_Details",
                url: "kompanija/detalji/{id}",
                defaults: new { controller = "Company", action = "Details" },
                constraints: new { id = "[0-9]+" }
            );

            routes.MapRoute(
                name: "Kompanije_Index",
                url: "kompanije",
                defaults: new { controller = "Company", action = "Index" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/Company/Pretraga/{query}",
                defaults: new { query = RouteParameter.Optional, controller = "Company", area = "API" }
            );
        }
    }
}
