﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vjezba.DAL.Repository;

namespace Vjezba.Web.Models
{
    public class CompanyFilterModel : ICompanyFilter
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
    }
}