﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Vjezba.DAL;
using Vjezba.DAL.Repository;
using Vjezba.Model;

namespace Vjezba.Web.Areas.API.Controllers
{
    public class CompanyController : ApiController
    {

        [Inject]
        public CompanyRepository CompanyRepository { get; set; }

        // GET: api/Company
        public IQueryable<CompanyDTO> Get()
        {
            return CompanyRepository.GetAllDTO();
        }

        // GET: api/Company/id
        public IEnumerable<CompanyDTO> Get(int id)
        {
            return CompanyRepository.GetDTO(id);
        }


        [System.Web.Http.ActionName("Pretraga")]
        public IQueryable<CompanyDTO> Get(string query)
        {
            return CompanyRepository.GetAllDTO(query);
        }

        [System.Web.Http.Route("api/company/Add")]
        public IHttpActionResult Post([FromBody]CompanyDTO value)
        {
            Company c = new Company()
            {
                Name = value.Name,
                Address = value.Address,
                CityID = value.CityID,
                DateFrom = value.DateFrom,
                Email = value.Email,
                Latitude = value.Latitude,
                Longitude = value.Longitude
            };
            CompanyRepository.Add(c, true);
            return this.Ok();
        }
    }
}