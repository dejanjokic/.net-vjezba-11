﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vjezba.Model
{
    public class CompanyContact : EntityBase
    {
        public string Value { get; set; }

        public ContactType ContactType { get; set; }

        [ForeignKey("Company")]
        public int CompanyID { get; set; }

        public virtual Company Company { get; set; }
    }

    public enum ContactType
    {
        Tel, Mob, Email
    }
}
